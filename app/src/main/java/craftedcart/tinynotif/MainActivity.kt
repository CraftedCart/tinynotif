package craftedcart.tinynotif

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.TypedValue
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.concurrent.schedule
import kotlin.math.round

const val NOTIF_CHANNEL_ID = "example_channel"
var lastNotifId = 0

class MainActivity : AppCompatActivity() {
    private var sharedPref: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createNotificationChannels()
        setContentView(R.layout.activity_main)


        Timer().schedule(6000, 6000) {
            // Run on main thread
            Handler(Looper.getMainLooper()).post {
                notificationView.invalidateTextPaintAndMeasurements()
            }
        }

        fontSizeSeekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                setFontSize((progress + 12).toFloat()) // 12 is the min for this seek bar
            }
        })

        scrollSpeedSeekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                setScrollSpeed(progress * 0.05f)
            }
        })

        sharedPref = PreferenceManager.getDefaultSharedPreferences(baseContext)

        // Load prefs
        val fontSize = sharedPref!!.getFloat(getString(R.string.pref_font_size_key), 16f)
        setFontSize(fontSize)
        val scrollSpeed = sharedPref!!.getFloat(getString(R.string.pref_scroll_speed_key), 0.3f)
        setScrollSpeed(scrollSpeed)
    }

    private fun createNotificationChannels() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.notification_channel_name)
            val descriptionText = getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH // For heads up notifs in Android 8 and above
            val channel = NotificationChannel(NOTIF_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }

            // Register the channel with the system
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    // Called when the user presses the send example notification button
    @Suppress("UNUSED_PARAMETER")
    fun sendExampleNotification(view: View) {
        val builder = NotificationCompat.Builder(this, NOTIF_CHANNEL_ID)
            .setSmallIcon(android.R.drawable.ic_dialog_info)
            .setContentTitle(resources.getString(R.string.example_notification_title))
            .setContentText(resources.getString(R.string.example_notification_text))
            .setDefaults(Notification.DEFAULT_ALL) // For heads up notifs in Android 7 and below
            .setPriority(NotificationCompat.PRIORITY_HIGH) // For heads up notifs in Android 7 and below
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setTimeoutAfter(10000)

        with(NotificationManagerCompat.from(this)) {
            notify(lastNotifId++, builder.build())
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun requestPermissions(view: View) {
        val intent = Intent(this, RequestPermissionsActivity::class.java)
        startActivity(intent)
    }

    @Suppress("UNUSED_PARAMETER")
    fun disableHeadsUp(view: View) {
        val intent = Intent(this, DisableHeadsUpActivity::class.java)
        startActivity(intent)
    }

    private fun setFontSize(size: Float) {
        fontSizeText.text = round(size).toInt().toString()
        fontSizeSeekBar.progress = round(size).toInt() - 12 // 12 is the min of this seek bar

        val dim = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, size, resources.displayMetrics)
        notificationView.textSize = dim

        val params = notificationView.layoutParams
        params.height = notificationView.minimumHeight
        notificationView.layoutParams = params

        notificationListenerInstance?.setFontSize(size)

        with (sharedPref!!.edit()) {
            putFloat(getString(R.string.pref_font_size_key), size)
            apply()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setScrollSpeed(speed: Float) {
        scrollSpeedText.text = "%.2f".format(speed)
        scrollSpeedSeekBar.progress = round(speed / 0.05f).toInt() // 0.05 is the snap interval

        notificationView.scrollSpeed = speed
        notificationListenerInstance?.setScrollSpeed(speed)

        with (sharedPref!!.edit()) {
            putFloat(getString(R.string.pref_scroll_speed_key), speed)
            apply()
        }
    }
}
