package craftedcart.tinynotif

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_request_permissions.*

class RequestPermissionsActivity : AppCompatActivity() {
    private var updateHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_permissions)

        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)

        updateButtons()

        updateHandler = Handler()
        startUpdateTask()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopUpdateTask()
    }

    private var updateTask: Runnable = object : Runnable {
        override fun run() {
            try {
                updateButtons()
            } finally {
                updateHandler!!.postDelayed(this, 1000)
            }
        }
    }

    private fun startUpdateTask() {
        updateTask.run()
    }

    private fun stopUpdateTask() {
        updateHandler!!.removeCallbacks(updateTask)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return true
    }

    private fun updateButtons() {
        val successColor = resources.getColorStateList(R.color.colorSuccess, null)
        val errorColor = resources.getColorStateList(R.color.colorError, null)

        val successIcon = resources.getDrawable(R.drawable.ic_check_white_24dp, null)
        successIcon.setBounds(0, 0, 60, 60)
        val errorIcon = resources.getDrawable(R.drawable.ic_close_white_24dp, null)
        errorIcon.setBounds(0, 0, 60, 60)

        if (Settings.canDrawOverlays(this)) {
            requestOverlayButton.backgroundTintList = successColor
            requestOverlayButton.setCompoundDrawables(successIcon, null, null, null)
        } else {
            requestOverlayButton.backgroundTintList = errorColor
            requestOverlayButton.setCompoundDrawables(errorIcon, null, null, null)
        }

        if (notificationListenerInstance != null) {
            requestNotificationAccessButton.backgroundTintList = successColor
            requestNotificationAccessButton.setCompoundDrawables(successIcon, null, null, null)
        } else {
            requestNotificationAccessButton.backgroundTintList = errorColor
            requestNotificationAccessButton.setCompoundDrawables(errorIcon, null, null, null)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun requestOverlayPermission(view: View) {
        val intent = Intent(
            Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
            Uri.parse("package:$packageName")
        )
        startActivity(intent)
    }

    @Suppress("UNUSED_PARAMETER")
    fun requestNotificationAccessPermission(view: View) {
        val intent = Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS)
        startActivity(intent)
    }
}
