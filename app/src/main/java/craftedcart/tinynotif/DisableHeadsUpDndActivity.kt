package craftedcart.tinynotif

import android.Manifest
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

class DisableHeadsUpDndActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disable_heads_up_dnd)

        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return true
    }

    @Suppress("UNUSED_PARAMETER")
    fun configureDnd(view: View) {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (!notificationManager.isNotificationPolicyAccessGranted) {
            Toast.makeText(baseContext, resources.getString(R.string.missing_notification_access_permission), Toast.LENGTH_LONG).show()
            return
        }

        // Suppress heads up notifications for do-not-disturb
        val suppressPolicy = if (Build.VERSION.SDK_INT >= 28) { // SDK 28 is Android 9
            NotificationManager.Policy.SUPPRESSED_EFFECT_PEEK
        } else {
            NotificationManager.Policy.SUPPRESSED_EFFECT_SCREEN_ON
        }

        notificationManager.notificationPolicy = NotificationManager.Policy(
            0,
            0,
            0,
            suppressPolicy
        )

        Toast.makeText(baseContext, resources.getString(R.string.success), Toast.LENGTH_SHORT).show()
    }

    @Suppress("UNUSED_PARAMETER")
    fun enableDndPriority(view: View) {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (!notificationManager.isNotificationPolicyAccessGranted) {
            Toast.makeText(baseContext, resources.getString(R.string.missing_notification_access_permission), Toast.LENGTH_LONG).show()
            return
        }

        notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_PRIORITY)
        Toast.makeText(baseContext, resources.getString(R.string.success), Toast.LENGTH_SHORT).show()
    }

    @Suppress("UNUSED_PARAMETER")
    fun enableDndAlarms(view: View) {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (!notificationManager.isNotificationPolicyAccessGranted) {
            Toast.makeText(baseContext, resources.getString(R.string.missing_notification_access_permission), Toast.LENGTH_LONG).show()
            return
        }

        notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_ALARMS)
        Toast.makeText(baseContext, resources.getString(R.string.success), Toast.LENGTH_SHORT).show()
    }

    @Suppress("UNUSED_PARAMETER")
    fun enableDndSilence(view: View) {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (!notificationManager.isNotificationPolicyAccessGranted) {
            Toast.makeText(baseContext, resources.getString(R.string.missing_notification_access_permission), Toast.LENGTH_LONG).show()
            return
        }

        notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_NONE)
        Toast.makeText(baseContext, resources.getString(R.string.success), Toast.LENGTH_SHORT).show()
    }
}
