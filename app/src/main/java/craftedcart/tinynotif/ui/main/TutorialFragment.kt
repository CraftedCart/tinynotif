package craftedcart.tinynotif.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import craftedcart.tinynotif.R

class TutorialFragment(private val tutorialSection: TutorialSection) : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_tutorial, container, false)

        val titleLabel: TextView = root.findViewById(R.id.titleLabel)
        titleLabel.text = resources.getString(tutorialSection.title)

        val infoLabel: TextView = root.findViewById(R.id.infoLabel)
        infoLabel.text = resources.getString(tutorialSection.info)

        val imageView: ImageView = root.findViewById(R.id.imageView)
        imageView.setImageResource(tutorialSection.image)

        return root
    }
}