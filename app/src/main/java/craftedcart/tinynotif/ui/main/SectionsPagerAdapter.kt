package craftedcart.tinynotif.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import craftedcart.tinynotif.R

private val TAB_TITLES = arrayOf(
    TutorialSection(
        R.string.tutorial_adb_enable_developer,
        R.string.tutorial_adb_enable_developer_info,
        R.drawable.press_build_number
    ),
    TutorialSection(
        R.string.tutorial_adb_enable_usb_debug,
        R.string.tutorial_adb_enable_usb_info,
        R.drawable.press_usb_debugging
    ),
    TutorialSection(
        R.string.tutorial_adb_install_android_tools,
        R.string.tutorial_adb_install_android_tools_info,
        R.drawable.platform_tools
    ),
    TutorialSection(
        R.string.tutorial_adb_connect_device_to_computer,
        R.string.tutorial_adb_connect_device_to_computer_info,
        R.drawable.connect_usb
    ),
    TutorialSection(
        R.string.tutorial_adb_transfer_files,
        R.string.tutorial_adb_transfer_files_info,
        R.drawable.transfer_files
    ),
    TutorialSection(
        R.string.tutorial_adb_devices,
        R.string.tutorial_adb_devices_info,
        R.drawable.adb_devices
    ),
    TutorialSection(
        R.string.tutorial_adb_disable_heads_up,
        R.string.tutorial_adb_disable_heads_up_info,
        R.drawable.adb_disable_heads_up
    )
)

class TutorialSection(
    val title: Int,
    val info: Int,
    val image: Int
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a TutorialFragment (defined as a static inner class below).
        return TutorialFragment(TAB_TITLES[position])
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(R.string.tutorial_step).format(position + 1)
    }

    // Number of pages
    override fun getCount(): Int {
        return TAB_TITLES.size
    }
}