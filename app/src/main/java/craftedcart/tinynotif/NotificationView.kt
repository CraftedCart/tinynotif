package craftedcart.tinynotif

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.Align
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorInt
import java.util.concurrent.TimeUnit
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.round

class NotificationView : View {
    private var _title: String = ""
    private var _text: String = ""
    private var _textColor: Int = Color.WHITE
    private var _textSize: Float = 0f
    private var _backgroundColor: Int = 0xDD212121.toInt()
    var icon: Drawable = context.resources.getDrawable(android.R.drawable.ic_dialog_info, null)
    var scrollSpeed: Float = 0.3f

    private var titleTextPaint: TextPaint? = null
    private var bodyTextPaint: TextPaint? = null
    private var backgroundDrawable: ColorDrawable? = null

    private var notifStartTime: Long = 0
    private var scrollStartTime: Long = 0

    private var textHeight = 0f
    private var textY = 0f
    private var titleTextWidth = 0f
    private var bodyTextWidth = 0f

    var title: String
        get() = _title
        set(value) {
            _title = value
            invalidateTextPaintAndMeasurements()
        }

    var text: String
        get() = _text
        set(value) {
            _text = value
            invalidateTextPaintAndMeasurements()
        }

    var textSize: Float
        get() = _textSize
        set(value) {
            _textSize = value
            invalidateTextPaintAndMeasurements()
        }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.NotificationView, defStyle, 0
        )

        a.getString(R.styleable.NotificationView_title)?.let {
            _title = it
        }

        a.getString(R.styleable.NotificationView_text)?.let {
            _text = it
        }

        a.getColor(R.styleable.NotificationView_textColor, _textColor).let {
            _textColor = it
        }

        a.getDimension(
            R.styleable.NotificationView_textSize,
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16f, resources.displayMetrics)
        ).let {
            _textSize = it
        }

        a.getColor(R.styleable.NotificationView_backgroundColor, _backgroundColor).let {
            _backgroundColor = it
        }

        a.recycle()

        // Set up a default TextPaint object
        titleTextPaint = TextPaint().apply {
            flags = Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG
            textAlign = Align.LEFT
            typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        }
        bodyTextPaint = TextPaint().apply {
            flags = Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG
            textAlign = Align.LEFT
        }

        backgroundDrawable = ColorDrawable(_backgroundColor)

        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements()
    }

    fun invalidateTextPaintAndMeasurements() {
        titleTextPaint?.let {
            it.textSize = _textSize
            it.color = _textColor

            val metric = it.fontMetrics
            textHeight = ceil(metric.descent - metric.ascent)
            textY = textHeight - metric.descent

            titleTextWidth = it.measureText(_title)
        }

        bodyTextPaint?.let {
            it.textSize = _textSize
            it.color = _textColor

            bodyTextWidth = it.measureText(_text)
        }

        minimumHeight = textHeight.toInt()

        notifStartTime = System.nanoTime()
        scrollStartTime = System.nanoTime()

        invalidate()
    }

    @ColorInt
    fun adjustAlpha(@ColorInt color: Int, factor: Float): Int {
        val alpha = round(Color.alpha(color) * factor).toInt()
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)

        return Color.argb(alpha, red, green, blue)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val contentWidth = width - paddingLeft - paddingRight
        val contentHeight = height - paddingTop - paddingBottom

        val iconSize = contentHeight - 4

        val maxScroll = if (titleTextWidth + 16f + bodyTextWidth + 16f + iconSize + 4f < contentWidth) {
            -4f
        } else {
            titleTextWidth + 16f + bodyTextWidth + 16f + iconSize + 4f - contentWidth
        }

        val nowTime = System.nanoTime()
        val scrollTimeDiff = nowTime - scrollStartTime
        val scrollTimeDiffMillis = max(0, TimeUnit.NANOSECONDS.toMillis(scrollTimeDiff) - 1000)
        val scrollOffset = max(maxScroll * -1f, 4f - (scrollTimeDiffMillis * scrollSpeed))

        // So we wait for 1000ms when we reach the end of scrolling
        val adjustedScrollOffset = max(maxScroll * -1f, 4f - ((scrollTimeDiffMillis - 1000) * scrollSpeed))
        if (maxScroll + adjustedScrollOffset <= 0f) {
            scrollStartTime = System.nanoTime()
        }

        // Fade out after a bit
        val notifTimeDiff = nowTime - notifStartTime
        val alphaTimeDiffMillis = max(0, TimeUnit.NANOSECONDS.toMillis(notifTimeDiff) - 4000)
        val alphaMult = max(0f, 1f - (alphaTimeDiffMillis / 1000f))
        backgroundDrawable!!.color = adjustAlpha(_backgroundColor, alphaMult)
        backgroundDrawable!!.let {
            it.setBounds(
                paddingLeft,
                paddingTop,
                paddingLeft + contentWidth,
                paddingTop + contentHeight
            )
            it.draw(canvas)
        }

        icon.alpha = (alphaMult * 0xFF).toInt()
        icon.setBounds(
            4 + scrollOffset.toInt(),
            4,
            contentHeight - 4 + scrollOffset.toInt(),
            contentHeight - 4
        )
        icon.draw(canvas)

        titleTextPaint!!.color = adjustAlpha(_textColor, alphaMult)
        bodyTextPaint!!.color = adjustAlpha(_textColor, alphaMult)
        canvas.drawText(_title, scrollOffset + iconSize + 16f, textY, titleTextPaint!!)
        canvas.drawText(_text, scrollOffset + iconSize + 16f + titleTextWidth + 16f, textY, bodyTextPaint!!)

        postInvalidate()
    }
}
