package craftedcart.tinynotif

import android.app.Notification
import android.content.Context
import android.content.SharedPreferences
import android.graphics.PixelFormat
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import androidx.preference.PreferenceManager
import java.util.*
import kotlin.concurrent.schedule

var notificationListenerInstance: NotificationListener? = null

class NotificationListener : NotificationListenerService() {
    private var notifView: NotificationView? = null
    private var layoutParams: WindowManager.LayoutParams? = null
    private var hideTimer: Timer? = null
    private var hasAddedOverlay = false
    private var sharedPref: SharedPreferences? = null

    override fun onCreate() {
        super.onCreate()

        // Add the overlay UI
        notifView = NotificationView(this)
        notifView!!.visibility = View.GONE

        if (Settings.canDrawOverlays(this)) {
            addOverlay()
        }

        sharedPref = PreferenceManager.getDefaultSharedPreferences(baseContext)

        // Load prefs
        val fontSize = sharedPref!!.getFloat(getString(R.string.pref_font_size_key), 16f)
        setFontSize(fontSize)
        val scrollSpeed = sharedPref!!.getFloat(getString(R.string.pref_scroll_speed_key), 0.3f)
        setScrollSpeed(scrollSpeed)

        notificationListenerInstance = this
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification?) {}

    override fun onDestroy() {
        super.onDestroy()

        notificationListenerInstance = null

        if (hasAddedOverlay) {
            val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.removeView(notifView)
        }
    }

    override fun onNotificationPosted(sbn: StatusBarNotification?) {
        if (!hasAddedOverlay && Settings.canDrawOverlays(this)) {
            addOverlay()
        }

        val notif = sbn!!.notification

        if ((notif.flags and Notification.FLAG_ONGOING_EVENT) == Notification.FLAG_ONGOING_EVENT) {
            return
        }

        val title = notif.extras.getCharSequence("android.title")
        var text = notif.extras.getCharSequence("android.text")
        if (title == null || text == null) return

        val lines = notif.extras.getCharSequenceArray("android.textLines")
        val icon = notif.smallIcon.loadDrawable(baseContext)

        if (lines != null && lines.isNotEmpty()) {
            text = lines.joinToString(separator = " / ")
        }

        notifView!!.let {
            it.title = title.toString()
            it.text = text.toString()
            it.visibility = View.VISIBLE
            it.icon = icon
        }

        // Hide notif after a little bit
        if (hideTimer != null) {
            hideTimer!!.cancel()
        }
        hideTimer = Timer()
        hideTimer!!.schedule(5000) {
            // Run on main thread
            Handler(Looper.getMainLooper()).post {
                notifView!!.visibility = View.GONE
            }
        }
    }

    private fun addOverlay() {
        val overlayType = if (Build.VERSION.SDK_INT >= 26) {
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY
        }

        layoutParams = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            overlayType,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            PixelFormat.TRANSLUCENT
        )

        layoutParams!!.gravity = Gravity.TOP or Gravity.FILL_HORIZONTAL
        layoutParams!!.x = 0
        layoutParams!!.y = 0
        layoutParams!!.height = notifView!!.minimumHeight // Don't stretch

        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.addView(notifView, layoutParams)
        hasAddedOverlay = true
    }

    fun setFontSize(size: Float) {
        if (notifView == null || layoutParams == null) return

        val dim = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, size, resources.displayMetrics)
        notifView!!.textSize = dim
        layoutParams!!.height = notifView!!.minimumHeight

        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.updateViewLayout(notifView!!, layoutParams!!)
    }

    fun setScrollSpeed(speed: Float) {
        if (notifView == null) return

        notifView!!.scrollSpeed = speed
    }
}